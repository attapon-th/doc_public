# TUC Register Api


[![ICTMOPH](https://ict.moph.go.th/assets/nodcms_general/img/logo.jpg)](https://www.ict.moph.go.th/)

---

##Description
 เป็น API การยืนยันผู้ติดเชื้อ HIV/AIDS ผ่านระบบ HDC
    
---
## 1. How to: `Register`
 วีธีการเรียกใช้งาน
 * Protocol: HTTP
 * Host: 203.157.102.60
 * Port: 80
 * Method: POST
 * Data-Form: {data}
 * URL: http://203.157.102.60/tuc/register/{hash}
 * Result: Json

### ข้อมูล Data ที่ส่งมา
ข้อมูล form-data ที่ส่งออกมา
***ข้อมูลอยู่ในรูปแบบ String ทั้งหมด***
1. hospcode => รหัส รพ.,ฯลฯ (5 หลัก) [PK]
2. cid => รหัส ปปช. (13 หลัก) [PK]
3. pid => รหัส (13 หลัก) [PK]
4. nation => สัญชาติ
5. date_register => วันที่สมัคร
6. user_cid => user cid

 **Example Data**

>       pid:0000000000000123
>       cid:3XXX048XXX8
>       nation:099
>       date_register:2019-02-03 15:02:12
>       user_cid:3XXX048XXX8


### การคำนวน hash

     hash = sha256(hospcode + cid + pid + token_string)

- token_string => จะกำหนดโดย Admin เฉพาะ

### Example
ตัวอย่างการเรียนใช้งานผ่าน HTTP


    http://203.157.102.60/tuc/regis/ee0785076641cb313fc9c2229087a769d09c08b9b5222117d00f2045f24fd685

### Result
ผลลัพธ์ของการเรียนเรียกใช้งาน

    {
        "data": [],
        "datetime": "2019-06-05 10:10:49",
        "msg": "Success Update Data",
        "success": true
    }

---
## 2. How to: `Reject`
 วีธีการเรียกใช้งาน
 * Protocol: HTTP
 * Host: 203.157.102.60
 * Port: 80
 * Method: POST
 * Data-Form: {data}
 * URL: http://203.157.102.60/tuc/reject/{hash}
 * Result: Json

### ข้อมูล Data ที่ส่งมา
ข้อมูล form-data ที่ส่งออกมา
***ข้อมูลอยู่ในรูปแบบ String ทั้งหมด***
1. hospcode => [PK] รหัส รพ.,ฯลฯ (5 หลัก) 
2. cid => รหัส ปปช. (13 หลัก) 
3. pid =>  [PK]  รหัส (13 หลัก) 
4. last_visit => [PK] วันที่ visit สุดท้าย (YYYY-MM-DD) ex. 2019-05-31
5. date_reject => วันที่ยกเลิก  (YYYY-MM-DD hh:mm:ss) ex. 2019-05-31 15:02:22
6. user_cid => user cid

 **Example Data**

>       hospcode:10660
>       pid:0000000000000123
>       cid:3XXX048XXX8
>       last_visit:2019-02-03
>       date_reject:2019-02-03 15:02:22
>       user_cid:3XXX048XXX8

###การคำนวน hash
        hash = sha256(hospcode + pid + last_visit + token_string)
- token_string => จะกำหนดโดย Admin เฉพาะ

### Example
ตัวอย่างการเรียนใช้งานผ่าน HTTP

    http://203.157.102.60/tuc/regis/9e74b34e492870ccb6149ef7aa4c7b50da9eca8ad7a0fe02ccc856780cd0c2a5

### Result
ผลลัพธ์ของการเรียนเรียกใช้งาน

    {
        "data": [],
        "datetime": "2019-06-05 10:11:50",
        "msg": "Success Update Data",
        "success": true
    }
