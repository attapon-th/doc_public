# EIIS Summary Api


[![ICTMOPH](https://ict.moph.go.th/assets/nodcms_general/img/logo.jpg)](https://www.ict.moph.go.th/)

---

##Description
 เป็น API สำหรับการร้องข้อมูล EIIS Summary ของแต่ละเดือน แยกตามรหัส HOSPCODE
    
---
## How to
 วีธีการเรียกใช้งาน
 * Protocol: HTTP
 * Host: 203.157.102.60
 * Port: 80
 * Method: GET
 * URL by HOSPCODE: `http://203.157.102.60/tuc/eiis_sum/{hospcode}/{year}/{month}/{hash}`
 * URL by ALL MONTH: `http://203.157.102.60/tuc/eiis_sum_full/{year}/{month}/{hash}`
 * Result: Json

### ส่วนประกอบของ URL
- hospcode => รหัส รพ, รพสต, ฯลฯ (5 หลัก)
- year => ปี ค.ศ. ของข้อมูล (4 หลักเต็ม)
- month => เดือน ของข้อมูล (2 หลักเต็ม)
- hash => คือการ Hash ข้อมูลด้วย SHA256 โดยการ

        hash = sha256(hospcode + year + month+ token_string)
- token_string => จะกำหนดโดย Admin เฉพาะ

### Example
ตัวอย่างการเรียนใช้งานผ่าน HTTP

    http://203.157.102.60/tuc/eiis_sum/10***/2017/09/066ad2cf183b111218fb53b2b4a4b31b7544adc974a8ada08dde87e1ab5c193d

### Result
ผลลัพธ์ของการเรียนเรียกใช้งาน

    {
        "data": [.....<จะเป็นข้อมูล EIIS Summary>.....],
        "datetime": "2019-06-04 10:11:30",
        "msg": "",
        "success": true
    }

---

## Example (PHP)

ตัวอย่างการทำงานของด้วยภาษา PHP

#### การขอข้อมูลเป็นราย โรงพยาบาลในเดือนนั้นๆ

```php
$hospcode = '10708';
$year = '2017';
$month ='09';
$token = 'X3hvxTvnzpvsmbpmQX2ec2PfCoz5ymLXc6BPiZZ4ayQEOxNkKMd13VCVz2XHgHnFZZOVQDC5cpBZKdduV1';

$token_sign = hash('sha256', $hospcode.$year.$month.$token );
$url = "http://203.157.102.60/tuc/eiis_sum/{$hospcode}/{$year}/{$month}/{$token_sign}";

echo $url;

# Get Data by HTTP GET
$response = file_get_contents($url);
$result = json_decode($response);
print_r($result);
```

#### การขอข้อมูลเป็นทั้งหมดในเดือนนั้นๆ 

*ปล. ข้อมูลมีขนานใหญ่และใช้เวลาประมวลผลนาน*

```php
$year = '2017';
$month ='09';
$token = 'X3hvxTvnzpvsmbpmQX2ec2PfCoz5ymLXc6BPiZZ4ayQEOxNkKMd13VCVz2XHgHnFZZOVQDC5cpBZKdduV1';

$token_sign = hash('sha256', $year.$month.$token);

$url = "http://203.157.102.60/tuc/eiis_sum_full/{$year}/{$month}/{$token_sign}";
	
echo $url;

# Get Data by HTTP GET
$response = file_get_contents($url);
$result = json_decode($response);
print_r($result);
```

